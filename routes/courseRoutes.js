const express = require("express");
const router = express.Router();
const auth = require("../auth.js");


const courseControllers = require("../controllers/courseControllers");

//Adding course
router.post("/" , auth.verify,courseControllers.addCourse);

// All active courses
router.get("/allActiveCourses", courseControllers.getAllActive);

//retrieve all courses
router.get("/allCourses", auth.verify, courseControllers.getAllCourses)

//Specific course
router.get("/:courseId", courseControllers.getCourse);

//update specific course
router.put("/update/:courseId", auth.verify, courseControllers.updateCourse)

//activity s40
router.patch("/:courseId/archive", auth.verify, courseControllers.updateCourseIsActive)




module.exports = router;