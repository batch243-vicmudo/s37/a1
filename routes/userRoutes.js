const express = require("express");
const router = express.Router();
const auth = require("../auth");



const userController = require("../controllers/userControllers");
	//Route for checking email
	router.post("/checkEmail", userController.checkEmailExists);
	//Route for registration
	router.post("/register", userController.checkEmailExists, userController.registerUser);
	//Route for login
	router.post("/login", userController.loginUser);
	//Route to display user info 
	router.post("/details",auth.verify, userController.retrieveUserDetails);
	//
	router.get("/profile", userController.profileDetails);
	//update role
	router.patch("/updateRole/:userId", auth.verify ,userController.updateRole)

	//enrollment
	router.post("/enroll/:courseId", auth.verify, userController.enroll)
module.exports = router;